///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file reportCats.c
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   22_02_2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int printCat(int catIndex){
   if(catIndex < 0 || catIndex > MAX_CATS) {
      printf("animalFarm0: Bad cat [%d]\n", catIndex);
      return 1;
   }
   printf("cat index = [%u] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", catIndex, name[catIndex], catGender[catIndex], catBreed[catIndex], isFixed[catIndex], weight[catIndex]);
   return 0;
}

int printAllCats(){
   for(int i = 0; i < numCats; i++){
       printf("cat index = [%u] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", i, name[i], catGender[i], catBreed[i], isFixed[i], weight[i]);
   }
   return 1;
}

int findCat(char scoutName[]) {
   for(int i = 0; i <= numCats; i++){
      if(strcmp(scoutName, name[i]) == 0){
         return i;
      }
   }
   printf("There is no cat with the name %s\n", scoutName);
   return -1;
}







