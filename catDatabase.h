///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file catDatabase.h
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   22_02_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include <stdbool.h>
#define MAX_CATS 100
#define MAX_CAT_NAME 30
enum gender{UNKNOWN_GENDER,MALE,FEMALE};
enum breed{UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
extern char name[][MAX_CAT_NAME]; //1st is index 2nd is max chars
extern bool isFixed[];
extern float weight[];
extern enum gender catGender[MAX_CATS];
extern enum breed catBreed[MAX_CATS];
extern int numCats;
extern void initializeDatabase();

