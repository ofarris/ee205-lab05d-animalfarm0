///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file deleteCats.c
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   22_02_2022
///////////////////////////////////////////////////////////////////////////////


#include "catDatabase.h"
#include "addCats.h"
#include "updateCats.h"
#include "deleteCats.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void deleteAllCats() {
   printf("All cats deleted\n");
   for(int i = 0; i<= MAX_CATS; i++){
      memset(name[i], '-', 10);
      isFixed[i] = false;
      weight[i] = 0;
      catGender[i] = UNKNOWN_GENDER;
      catBreed[i] = UNKNOWN_BREED;
   }
}

void deleteCat( int index ) {
   for(int i = 0; i<= MAX_CATS; i++) {
      if(i == index) {
         for(int j = i; j <= (numCats - 1); j++){
            strcpy(name[j], name[j+1]);
            isFixed[j] = isFixed[j+1];
            weight[j] = weight[j+1];
            catGender[j] = catGender[j+1];
            catBreed[j] = catBreed[j+1];
         }
      }
   }
}

