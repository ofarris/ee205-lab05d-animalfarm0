///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file catDatabase.c 
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   22_02_2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
char name[MAX_CATS][MAX_CAT_NAME]; //1st is index 2nd is max chars
bool isFixed[MAX_CATS];
float weight[MAX_CATS];
enum gender catGender[MAX_CATS];
enum breed catBreed[MAX_CATS];
int numCats;
void initializeDatabase() {
   numCats = 0;
   for(int i = 0; i<= MAX_CATS; i++){
      memset(name[i], '-', 1);
      isFixed[i] = false;
      weight[i] = 0;
      catGender[i] = UNKNOWN_GENDER;
      catBreed[i] = UNKNOWN_BREED;
   }
}
