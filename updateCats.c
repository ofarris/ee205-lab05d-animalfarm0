///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file updateCats.c
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   22_02_2022
///////////////////////////////////////////////////////////////////////////////


#include "catDatabase.h"
#include "addCats.h"
#include "updateCats.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int updateCatName(int index, char newName[]) {
   for(int i = 0; i < numCats; i++){
      if(strcmp(newName, name[i]) == 0){
         printf("Failure: name must be unique\n");
         return 1;
      }
      if(strlen(newName) == 0) {
         return 1;
      }
   }
   strcpy(name[index], newName);
   return 0;
}

void fixCat(int index){
   isFixed[index] = true;
}

int updateCatWeight(int index, float newWeight){
   if(newWeight < 0) {
      printf("Failure: weight needs to be greater than or equal to zero\n");
      return 1;
   }
   weight[index] = newWeight;
   return 0;
}
