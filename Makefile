###############################################################################
#         University of Hawaii, College of Engineering
# @brief  Lab 05d - Countdown - EE 205 - Spr 2022
#
# @file    Makefile
# @version 1.0
#
# @author Oze Farris <ofarris@hawaii.edu>
# @date   22_02_2022
#
# @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

TARGET = animalFarm


all:  $(TARGET)


CC     = gcc
CFLAGS = -Wall -Wextra $(DEBUG_FLAGS)


debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean $(TARGET)

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c addCats.h
	$(CC) $(CFLGAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

animalFarm.o: animalFarm.c catDatabase.h addCats.h reportCats.h updateCats.h deleteCats.h
	$(CC) $(CFLAGS) -c animalFarm.c 
	

animalFarm: animalFarm.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) animalFarm.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o


test: $(TARGET)
	./$(TARGET)


clean:
	rm -f $(TARGET) *.o

