///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
//
// Usage:  animalFarm0
//
// @file animalFarm.c
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   22_02_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include "catDatabase.h"
#include <stdbool.h>
#include <string.h>
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"

int main() {
   initializeDatabase();
   addCats( "Loki", MALE, PERSIAN, true, 8.5 ) ;
   addCats( "Milo", MALE, MANX, true, 7.0 ) ;
   addCats( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
   addCats( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
   addCats( "Trin", FEMALE, MANX, true, 12.2 ) ;
   addCats( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
   
   printAllCats();
   printf("\n");  
   int kali = findCat( "Kali" ) ;
   printf("kali index is %d\n", kali);
   printf("\n");
   updateCatName( kali, "Chili" ) ; // this should fail
   printCat( kali );
   updateCatName( kali, "Capulet" ) ;
   updateCatWeight( kali, 9.9 ) ;
   fixCat( kali ) ;
   printCat( kali );
   printf("\n");
   printf("Here are all the cats\n");
   printAllCats();
   printf("\n"); 
   printf("Delete cat at index 2 and print all cats\n");
   deleteCat(2);
   printAllCats();
   printf("\n");
   deleteAllCats();
   printAllCats();
   printf("\n");
}
