///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalFarm0 - EE 205 - Spr 2022
//
// Usage:  animalFarm
//
// @file addCats.c
// @verision 1.0
//
// @author Oze Farris <ofarris@hawaii.edu>
// @date   22_02_2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#include "addCats.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int isValid(char checkName[],float checkWeight){
   if(numCats >= MAX_CATS) {
      return 1;
   }
   if(strlen(checkName) == 0) {
      return 1;
   }
   if(strlen(checkName) > MAX_CAT_NAME) {
      return 1;
   }
   if(checkWeight < 0){
      return 1;
   }
   for(int i=0; i<numCats; i++){
      if(strcmp(checkName, name[i]) == 0){
         return 1;
      }
   }
   return 0;
}   
int addCats(char addName[],enum gender addGender,enum breed addBreed,bool addIsFixed,float addWeight) {
   if(isValid(addName, addWeight) != 0) {
      return 1;
   }
   strcpy(name[numCats], addName); 
   isFixed[numCats] = addIsFixed;
   weight[numCats] = addWeight;
   catGender[numCats] = addGender;
   catBreed[numCats] = addBreed;
   numCats++;
   return numCats;
}
